import curses
import sys
import requests

from argparse import ArgumentParser
from pathlib import Path
from pytube import YouTube


screen = curses.initscr()
kilobyte = 0.00097656  # = 1 byte


def download_progress(stream, chunk, bytes_remaining):
    filesize_kB = round(stream.filesize*kilobyte, 2)
    remaining_kB = round(filesize_kB - (bytes_remaining*kilobyte), 2)
    screen.addstr(11, 1, str(remaining_kB) + "/" + str(filesize_kB) + "/kB")
    screen.refresh()


def download_done(stream, file_handle):
    screen.addstr(12, 1, "Done")
    screen.refresh()
    screen.clear()


class Download:
    def __init__(self, link_file, download_to, youtube_itag):
        self.link_file = link_file
        self.download_dir = download_to
        self.youtube_itag = youtube_itag

        with open(self.link_file, "r") as linklist_txt:
            self.linklist = linklist_txt.readlines()

    def bloody(self, stream):
        screen.addstr(8, 1, "File Size Approx: " + str(round(stream.filesize_approx*kilobyte, 2)) + "/kB")
        screen.addstr(9, 5, "File Size: " + str(round(stream.filesize*kilobyte, 2)) + "/kB")
        screen.refresh()
        stream.download(
            output_path=self.download_dir,
            skip_existing=True
        )

    def go(self):
        for link in self.linklist:
            link_e = format(link.strip())

            screen.addstr(1, 1, "Downloading: " + link_e + "...")
            screen.refresh()

            yt = YouTube(
                url=link_e,
                on_progress_callback=download_progress,
                on_complete_callback=download_done
            )

            screen.clear()
            screen.addstr(1, 1, "Downloading: " + yt.title)
            screen.addstr(2, 6, "Author: " + yt.author)
            screen.addstr(3, 6, "Length: " + str(round(yt.length/60, 2)) + "/min")
            screen.addstr(4, 5, "Ratings: " + str(round(yt.rating, 2)))
            screen.addstr(5, 7, "Views: " + str(yt.views))
            screen.addstr(6, 9, "Age: " + str(yt.age_restricted))
            screen.refresh()

            try:  # Example itags: 22 > 720p, 18 > 360p
                stream = yt.streams.get_by_itag(self.youtube_itag)
                self.bloody(stream)
            except AttributeError:  # In case video cannot be downloaded in given itag
                stream = yt.streams.get_highest_resolution()
                self.bloody(stream)


def check_connection(url):
    try:
        requests.get(url)
    except requests.exceptions.ConnectionError:
        curses.endwin()
        print("Error: Unable to connect to " + url + "!")
        sys.exit()


def main():
    curses.endwin()
    aparse = ArgumentParser(
        prog=sys.argv[0],
        usage=sys.argv[0] + " [ file | dir | itag ]",
        description="Youtube Downloader using pytube.",
        epilog="Copyright (c) 2020, Dow Van Dine, See LICENSE for more information."
    )
    aparse.add_argument("file", help="| List file")
    aparse.add_argument("dir", help="| Download dir")
    aparse.add_argument("itag", help="| Youtube itag")

    args = aparse.parse_args()

    if not Path(args.file).is_file():
        curses.endwin()
        print("Error: " + args.file + " is not a file!")
        sys.exit()

    if not Path(args.dir).is_dir():
        curses.endwin()
        print("Error: " + args.dir + " is not a dir!")
        sys.exit()

    check_connection("https://youtube.com")
    curses.curs_set(1)
    ytd = Download(args.file, args.dir, args.itag)
    ytd.go()


if __name__ == "__main__":
    main()
    curses.curs_set(0)
    curses.endwin()
