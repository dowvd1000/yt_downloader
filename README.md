# [YTD](https://gitlab.com/dowvd1000/yt_downloader)

Copyright (c) 2020 [Dow Van Dine](https://dow.vandine.xyz)  
See [LICENSE](https://gitlab.com/dowvd1000/yt_downloader/-/blob/master/LICENSE)

A Youtube Downloader using [pytube](https://github.com/nficano/pytube).

**Installation**

`pip3 install git+git://github.com/nficano/pytube.git`  
`pip3 install requests curses` and if on Windows `windows-curses`

**Usage**

`python3 ytd.py linkfile.txt /path/to/downloads itag`

Place your Youtube links in a file, with each link on a new line.  
See pytube [example](https://python-pytube.readthedocs.io/en/latest/user/quickstart.html#working-with-streams) on how to find Youtube itag.
